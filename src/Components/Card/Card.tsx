import React from 'react'
import './card.scss'
import star from '../../assets/star.png'

interface CardProps {
    item: any

}

export const Card: React.FC<CardProps> = ({ item }) => {
    return (
        <div className='card'>
            <img src={item.image} alt="product" />
            {/* limit title to 35 */}
            <h3>{item.title.length > 25 ? item.title.substring(0, 25) + '...' : item.title}</h3>
            {/* description limit to 50 letters */}
            <p>{item.description.length > 60 ? item.description.substring(0, 60) + '...' : item.description}</p>
            <div className='rating_price'>
                <p className='price'>${item.price}</p>
                <div className='rating'>
                    {/* star logo */}
                    <img src={star} alt="star"
                        style={{ width: '15px', height: '15px' }}
                    />
                    <p>{item.rating.rate}</p>
                </div>
            </div>
        </div>
    )
}
