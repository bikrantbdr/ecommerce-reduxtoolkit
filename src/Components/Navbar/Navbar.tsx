import './nav.scss'

const Navbar = () => {
    return (
        <nav>
            <div className="logo">Shoppie</div>
            <div className="links">
                <a href="/">Home</a>
                <a href="/">Products</a>
                <a href="/">About</a>
                <a href="/">Contact</a>
            </div>
        </nav>
    )
}

export default Navbar