
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { fetchProducts } from './Store/Slice/productSlice'
import { AppDispatch } from './Store/store';
import './App.scss'
import { Card } from './Components/Card/Card';
import Navbar from './Components/Navbar/Navbar';

function App() {

  const dispatch = useDispatch<AppDispatch>();
  const data = useSelector((state: any) => state.product.data)

  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  console.log(data)

  return (
    <>
      <Navbar />

      <div className='cardContainer'>
        {data.map((item: any) => (
          <Card item={item} key={item.id} />
        ))}
      </div>
    </>
  )
}

export default App
