import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchProducts = createAsyncThunk(
    'product/fetchProducts',
    async () => {
        const response = await fetch('https://fakestoreapi.com/products');
        return response.json() as Promise<any>;
    }
);



const productSlice = createSlice({
    name: 'product',
    initialState: {
        isLoading: false,
        data: [],
        isError: false,
    },
    extraReducers: (builder) => {
        builder.addCase(fetchProducts.pending, (state) => {
            state.isLoading = true;
            state.isError = false;
        });
        builder.addCase(fetchProducts.fulfilled, (state, action) => {
            state.isLoading = false;
            state.data = action.payload;
        });
        builder.addCase(fetchProducts.rejected, (state) => {
            state.isLoading = false;
            state.isError = true;
        });
    },
    reducers: {}
});


export default productSlice.reducer;